curl -LO https://github.com/jacobly0/llvm-project/archive/refs/heads/z80.zip
unzip z80.zip
cd llvm-project-z80 && patch -p1 < ../0001-Emit-GAS-sytax.patch
cd llvm-project-z80 \
    && mkdir build \
    && cmake -G Ninja -DLLVM_ENABLE_PROJECTS="clang" \
                      -DCMAKE_INSTALL_PREFIX=/opt/local/ez80-none-elf \
                      -DCMAKE_BUILD_TYPE=Release \
                      -DLLVM_EXPERIMENTAL_TARGETS_TO_BUILD=Z80 \
                      -DLLVM_TARGETS_TO_BUILD= \
                      -DLLVM_DEFAULT_TARGET_TRIPLE=ez80-none-elf \
                      ../llvm-project-z80/llvm \
    && ninja install
curl -LO https://mirror.freedif.org/GNU/binutils/binutils-2.36.1.tar.xz
tar xf binutils-2.36.1.tar.xz
cd binutils-2.36.1 \
    && ./configure --target=z80-none-elf --program-prefix=ez80-none-elf- --prefix=/opt/local/ez80-none-elf \
    && make -j4 \
    && make install