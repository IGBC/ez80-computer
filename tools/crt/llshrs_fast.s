.assume	adl=1

section	.text
.global	__llshrs_fast
__llshrs_fast:
; Could use optimization for shifting whole bytes at a time.
	ld	iy, 0
	add	iy, sp

	ld	a, c
	ld	c, b
	ld	b, (iy + 3)
	inc	b
	dec	b
	jr	z, .skip

	push	de
	push	hl

.loop:
	sra	c
.global	__llshrs_fast.hijack_llshru
__llshrs_fast.hijack_llshru:
	rra
	rr	(iy - 1)
	rr	d
	rr	e
	rr	(iy - 4)
	rr	h
	rr	l
	djnz	.loop

	ld	b, e
	ld	(iy - 2), d

	ex	de, hl
	pop	hl
	ld	l, e
	ld	h, d

	pop	de
	ld	e, b

.skip:
	ld	b, c
	ld	c, a

	ret

