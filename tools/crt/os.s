.assume	adl=1

section	.text

.global	__fadd
__fadd      := 000270h
.global	__fcmp
__fcmp      := 000274h
.global	__fdiv
__fdiv      := 000278h
.global	__fmul
__fmul      := 000288h
.global	__fsub
__fsub      := 000290h
.global	__ftol
__ftol      := 00027Ch
.global	__ftoul
__ftoul     := __ftol
.global	__dtol
__dtol      := __ftol
.global	__dtoul
__dtoul     := __ftoul
.global	__imul_b
__imul_b    := 000150h
.global	__indcall
__indcall   := 00015Ch
.global	__ishl_b
__ishl_b    := 000178h
.global	__ishrs_b
__ishrs_b   := 000180h
.global	__ishru_b
__ishru_b   := 000188h
.global	__itol
__itol      := 000194h
.global	__ltof
__ltof      := 000284h
.global	__ltod
__ltod      := __ltof
.global	__setflag
__setflag   := 000218h
.global	__sshl_b
__sshl_b    := 000244h
.global	__sshrs_b
__sshrs_b   := 00024Ch
.global	__sshru_b
__sshru_b   := 000254h
.global	__stoi
__stoi      := 000260h
.global	__stoiu
__stoiu     := 000264h
.global	__ultof
__ultof     := 000280h
.global	__ultod
__ultod     := __ultof
