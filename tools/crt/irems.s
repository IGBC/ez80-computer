.assume	adl=1

section	.text
.global	__irems

__irems:
; I: UHL=dividend, UBC=divisor
; O: uhl=UHL%UBC

	push	de

	call	__idvrms
	ex	de, hl

	pop	de
	ret


	extern	__idvrms
