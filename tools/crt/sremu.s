.assume	adl=1

section	.text
.global	__sremu

__sremu:
; I: HL=dividend, BC=divisor
; O: uhl=HL%BC

	push	de

	call	__sdvrmu

	pop	de
	ret

	extern	__sdvrmu
