.assume	adl=1

section	.text
.global	__sdivu

__sdivu:
; I: HL=dividend, BC=divisor
; O: uhl=HL/BC

	push	de

	call	__sdvrmu
	ex	de, hl

	pop	de
	ret

	extern	__sdvrmu