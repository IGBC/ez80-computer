.assume	adl=1

section	.text
.global	__iremu

__iremu:
; I: UHL=dividend, UBC=divisor
; O: uhl=UHL%UBC

	push	de

	call	__idvrmu

	pop	de
	ret

	extern	__idvrmu

