#![no_std]
#![no_main]

use panic_halt as _;
use stm32ral::{write_reg, read_reg, modify_reg};
use stm32f4xx_hal as hal;
use core::cell::{Cell, RefCell};
use cortex_m::interrupt::Mutex;
use stm32f4xx_hal::prelude::*;
use hal::interrupt;

type Button = hal::gpio::PC13<hal::gpio::Input>;

static BUTTON: Mutex<RefCell<Option<Button>>> = Mutex::new(RefCell::new(None));
static CLOCK: Mutex<Cell<bool>> = Mutex::new(Cell::new(false));


#[cortex_m_rt::entry]
unsafe fn main() -> ! {
    // Set up RCC. HSI/16=2MHz input, *100=200MHz VCO, /4=50MHz SYSCLK
    write_reg!(stm32ral::rcc, RCC, PLLCFGR, PLLM:4, PLLN: 80, PLLP: Div8, PLLSRC: HSI);
    // Enable PLL, wait for ready
    modify_reg!(stm32ral::rcc, RCC, CR, PLLON: On);
    while read_reg!(stm32ral::rcc, RCC, CR, PLLRDY != Ready) {}
    // Set MCO2 to PLL without division
    modify_reg!(stm32ral::rcc, RCC, CFGR, MCO2: PLL, MCO2PRE: Div1);

    // Enable GPIOA
    modify_reg!(stm32ral::rcc, RCC, AHB1ENR, GPIOCEN: Enabled);

    // Configure PA9 for MCO2 (AF0)
    modify_reg!(stm32ral::gpio, GPIOC, MODER, MODER9: Alternate, MODER11: Output);
    modify_reg!(stm32ral::gpio, GPIOC, OSPEEDR, OSPEEDR9: VeryHighSpeed, OSPEEDR11: VeryHighSpeed);
    modify_reg!(stm32ral::gpio, GPIOC, AFRH, AFRH9: AF0);

    // //Hal configures button
    // let mut dp = hal::pac::Peripherals::take().unwrap();
    // let gpioc = dp.GPIOC.split();
    // let mut button = gpioc.pc13;

    // let mut syscfg = dp.SYSCFG.constrain();
    // button.make_interrupt_source(&mut syscfg);
    // button.trigger_on_edge(&mut dp.EXTI, hal::gpio::Edge::Rising);
    // button.enable_interrupt(&mut dp.EXTI);

    // unsafe {
    //     cortex_m::peripheral::NVIC::unmask(button.interrupt());
    // }

    // cortex_m::interrupt::free(|cs| {
    //     BUTTON.borrow(cs).replace(Some(button))
    // });

    // loop {
    //     cortex_m::interrupt::free(|cs| {
    //         let clk = CLOCK.borrow(cs).get();
    //         if clk {
    //             modify_reg!(stm32ral::gpio, GPIOC, ODR, ODR11: High);
    //             CLOCK.borrow(cs).replace(false);
    //         }
    //     });
    //     cortex_m::asm::delay(50);
    //     modify_reg!(stm32ral::gpio, GPIOC, ODR, ODR11: Low);
    //     cortex_m::asm::delay(50);
    
    // }
    loop {
        let delay = 100000;
        modify_reg!(stm32ral::gpio, GPIOC, ODR, ODR11: High);
        cortex_m::asm::delay(delay/2);
        modify_reg!(stm32ral::gpio, GPIOC, ODR, ODR11: Low);
        cortex_m::asm::delay(delay/2);
    }
}

#[interrupt]
fn EXTI15_10() {
    cortex_m::interrupt::free(|cs| {
        CLOCK.borrow(cs).replace(true);
        let mut button = BUTTON.borrow(cs).borrow_mut();
        button.as_mut().unwrap().clear_interrupt_pending_bit();
    });
}