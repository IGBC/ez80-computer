int printf(const char *, ...);

/* Defines taken from lobject.c Lua 5.4.4 as test*/
#define RETS	"..."
#define PRE		"[string \""
#define POS		"\"]"

int puts(char *str);

__attribute__((__interrupt__))
void interrupt_code_generation_test(void)
{
	int	a;
	int	b;
	int	c;

	a = 1;
	b = 2;
	c = a + b;
} /* end interrupt_code_generation_test */

void non_interrupt_code_generation_test(void)
{
	int	a;
	int	b;
	int	c;

	a = 1;
	b = 2;
	c = a + b;
} /* end non_interrupt_code_generation_test */

int main(void)
{
	puts("Hello from Clang on the eZ80!\n");

	int	a;
	int	b;
	int	c;

	a = 1;
	b = 2;
	c = a + b;

	printf("%s\n", RETS);
	printf("%s\n", PRE);
	printf("%s\n", POS);

	return c;
} /* end main */
