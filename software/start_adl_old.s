.org 00h
.ASSUME ADL=0
    JP.LIL start

.fill 0Ah - $
    .DW int_default ; PRT0
    .DW int_default ; PRT1
    .DW int_default ; PRT2
    .DW int_default ; PRT3
    .DW int_default ; PRT4
    .DW int_default ; PRT5
    .DW int_default ; RTC
    .DW int_UART0
    .DW int_default ; UART1
    .DW int_default ; I2C
    .DW int_default ; SPI
.fill 30h - $
    .DW int_default ; PORT B0
    .DW int_default ; PORT B1
    .DW int_default ; PORT B2
    .DW int_default ; PORT B3
    .DW int_default ; PORT B4
    .DW int_default ; PORT B5
    .DW int_default ; PORT B6
    .DW int_default ; PORT B7
    .DW int_default ; PORT C0
    .DW int_default ; PORT C1
    .DW int_default ; PORT C2
    .DW int_default ; PORT C3
    .DW int_default ; PORT C4
    .DW int_default ; PORT C5
    .DW int_default ; PORT C6
    .DW int_default ; PORT C7
    .DW int_default ; PORT D0
    .DW int_default ; PORT D1
    .DW int_default ; PORT D2
    .DW int_default ; PORT D3
    .DW int_default ; PORT D4
    .DW int_default ; PORT D5
    .DW int_default ; PORT D6
    .DW int_default ; PORT D7
.fill 68h - $
.ASSUME ADL=1
int_default:
    jp error

.fill 70h - $
.DB "eZ80 SB Firmware"


int_UART0:
    push AF
    ld a, 'e'
    out0 (00C0h), A
    ld a, 'Z'
    out0 (00C0h), A
    ld a, '8'
    out0 (00C0h), A
    ld a, '0'
    out0 (00C0h), A
    in0 a, (009Ah)
    XOR a, 01h;
    out0 (009Ah), A
    pop AF
    reti

start:
    ; set bottom of RAM to 2M
    LD A, 80h
    OUT0 (00ABh), A
    ; set top of ram to 4M
    LD A, 9Fh
    OUT0 (00ACh), A
    ; enable ram with 0WS
    LD A, 08h  ; write WS0 IO0 EN1
    OUT0 (00ADh), A
    ; shrink CSO so ram is unmasked
    LD A, 07h ; rom ends at 512K
    OUT0 (00A9h), A
    ; change rom settings
    LD A, 08h  ; write WS2 IO0 EN1
    OUT0 (00AAh), A
    

    LD SP, 9FFFFFh ; set stack pointer to the top of ram
    
    LD IX, 800000h

    ld a, 00h ; load pin 4 as output
    out0 (009Bh), A ; Write the value to the PORT B direction Register
    ld a, 00h ; write led low
    out0 (009Ah), A

;clear_ram:
;    ld a, 000h ; write led low
;    out0 (009Ah), A
;    ; Setup ram clear 
;    ld HL, 3FFFFFh ; Start at top of mem
;    ld BC, 200000h ; run trough all of mem
;    ld DE, 009Ah ; load the value we just loaded (0)
;    INDRX

    ld hl, 0A00000h
;test_ram:
;    dec hl
;    ld a, 0FFh
;    ld (hl), a
;    sub a, (hl)
;    ;jp nz, error
;    ;ld a, 0
;    ;ld (hl), A
;    ;tst a, (hl)
;    ;jp nz, error
;    ld bc, 7FFFFFh
;    ld d, h
;    ld e, l
;    SBC hl, bc
;    ex de, hl
;    jr nz, test_ram

setup_uart:
    ; set PD0 as alt mode
    ld a, 03h ; set bit 0
    out0 (00A2h), a ; PD_DR activate PD0
    out0 (00A3h), a ; PD_DDR activate PD0
    out0 (00A5h), a ; PD_ALT2 activate PD0 
    ld a, 0h 
    out0 (00A4h), a ; PD_ALT1 clear PD0 
    

    ld a, 0C0h
    out0 (00C3h), A ;activate access to BRG0
    
    ;set the uart divisor to 26 (9615 baud @ 4M clk)
    ;ld a, 00
    ;out0 (00C1h), A ; BRG0H
    ld a, 26
    out0 (00C0h), A ; BRG0L

    ld a, 03h ; No test settings, mode 8N1
    out0 (00C3h), A ; reactivate uart

    ld a, 06h ; enable fifo clear buffers
    out0 (00C2h), A ; UART0_FCTL

print_wakeup:
    ld hl, wakeup_message
prloop: ld a, (hl)
    and a, A
    jr z, signal_done
prwait: ld c, 0C5h
    in a, (c)
    and a, 20h
    jr z, prwait
prout: ld c, 0C0h
    ld a, (hl)
    out (c), A
    inc hl
    jr prloop




    ;ei
    ;ld a, 02h
    ;out0 (00C1h), A

signal_done:
    ld bc, 1
    
    in0 a, (009Ah)
    xor a, 10h ; toggle led bit
    out0 (009Ah), A
    
    ; ld a, 00h
    ; out0 (00A3h), a ; PD_DDR activate PD0
    ; in0 a, (00A2h)
    ; xor a, 0FFh ; toggle led bit
    ; out0 (00A2h), A
    
    ld hl, 100000
loop_done: sbc hl, bc  ; sub 1
    jr nz, loop_done
    
    jr print_wakeup

llcall_print_msg:
prloop: ld a, (hl)
    and a, A
    jr z, prdone
prwait: ld c, 0C5h
    in a, (c)
    and a, 20h
    jr z, prwait
prout: ld c, 0C0h
    ld a, (hl)
    out (c), A
    inc hl
    jr prloop
prdone:
    jp (IX)

error:
    ld a, 10h
    out0 (009Ah), A
    DI
    halt

wakeup_message: .db "Hello eZ80!", 10, 13 ,0

;.fill 80000h - $