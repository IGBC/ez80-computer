; Initialises the PORTB register and manually blinks the LED on PB4

;.ASSUME ADL=0

;.ORD 0h

start:
    ld a, 0EFh ; load pin 4 as output
    ld B, 00h
    ld C, 09Bh;
    out (C), A ; Write the value to the PORT B direction Register
    ld B, 00h
    ld C, 09Ah;
    ld IX, 000000h;
loop:
    ld a, 010h ; write led high
    out (C), A
    
    ld a, 00h ; write led low
    out (C), A    
    jp loop