TARGET := start_clang

BUILD_DIR := ./build
SRC_DIRS := .

CFLAGS := -target ez80-none-elf -mllvm -z80-print-zero-offset -Wa,-march=ez80 -nostdinc
LDFLAGS := -T ez80.ld


PATH := ../../tools/ez80-llvm-toolchain/toolchain/ez80-none-elf/bin/
PREFIX := ez80-none-elf-

ROMCHIP := SST39LF040@PLCC32

CRT_DIR := ../../tools/crt
CRT := bbitrev.s bctlz.s bdivs.s bdivu.s bdvrms_abs.s bdvrmu.s bmuls.s bmulu.s bpopcnt.s brems.s bremu.s bshl.s bshrs.s bshru.s fpneg.s fpupop1.s fpupop2.s frameset0.s frameset.s frbmuls.s frimuls.s frimulu.s frsmuls.s frsmulu.s iand_fast.s iand.s ibitrev_fast.s ibitrev.s icmpzero.s ictlz.s idivs.s idivu.s idvrms.s idvrmu.s imulu_fast.s imulu.s inchar.s indcallhl.s ineg_fast.s ineg.s inot_fast.s inot.s internal_bitrev_byte.s ior_fast.s ior.s ipopcnt_fast.s ipopcnt.s irems.s iremu.s ishl.s ishrs_1_fast.s ishrs.s ishru_1_fast.s ixor_fast.s ixor.s labs.s ladd_1.s ladd_b.s ladd_fast.s ladd.s land_fast.s land.s lbitrev.s lbswap.s lcmps_fast.s lcmps.s lcmpu_fast.s lcmpu.s lcmpzero.s lctlz.s ldivs_lrems_common.s ldiv.s ldivs.s ldivu.s ldvrmu.s llabs.s lladd_1.s lladd_b_fast.s lladd_b.s lladd_fast.s lland_fast.s lland.s llbitrev.s llbswap.s llcmps_fast.s llcmps.s llcmpu_fast.s llcmpu.s llcmpzero_fast.s llcmpzero.s llctlz.s lldivu_b.s lldivu.s lldvrmu.s llmulu_b.s llneg_fast.s llneg.s llnot_fast.s llnot.s llor_fast.s llor.s llpopcnt_fast.s llpopcnt.s llremu.s llshl_1_fast.s llshl.s llshrs_1_fast.s llshrs_fast.s llshrs.s llshru_1_fast.s llshru_fast.s llshru.s llsub_1.s llsub_fast.s llsub.s llxor_fast.s llxor.s lmulu_fast.s lmulu.s lneg_fast.s lneg.s lnot_fast.s lnot.s lor_fast.s lor.s lpopcnt_fast.s lpopcnt.s lrems.s lremu.s lshl.s lshrs_1_fast.s lshrs.s lshru_1_fast.s lshru.s lsub_1.s lsub_fast.s lsub.s lxor_fast.s lxor.s popcnt_common.s sand_fast.s sand.s sbitrev.s scmpzero.s sctlz.s sdivs.s sdivu.s sdvrms_abs.s sdvrmu.s smulu_fast.s smulu.s sneg_fast.s sneg.s snot_fast.s snot.s sor_fast.s sor.s spopcnt_fast.s spopcnt.s srems.s sremu.s sshl.s sshrs.s sshru.s sxor_fast.s sxor.s
# ladd_b_fast.s lladd.s lldivs.s llmulu.s lltof.s ulltof.s lldiv.s
CRT_SRCS := $(CRT:%=$(CRT_DIR)/%)
CRT_OBJS := $(CRT:%=$(BUILD_DIR)/%.o)
# Find all the C and C++ files we want to compile
# Note the single quotes around the * expressions. The shell will incorrectly expand these otherwise, but we want to send the * directly to the find command.
# SRCS := $(shell find $(SRC_DIRS) -name '*.cpp' -or -name '*.c' -or -name '*.s')
SRCS := bios_adl.s ez80.c

# Prepends BUILD_DIR and appends .o to every src file
# As an example, ./your_dir/hello.cpp turns into ./build/./your_dir/hello.cpp.o
OBJS := $(SRCS:%=$(BUILD_DIR)/%.o) $(CRT_OBJS)


# String substitution (suffix version without %).
# As an example, ./build/hello.cpp.o turns into ./build/hello.cpp.d
#DEPS := $(OBJS:.o=.d)

# Every folder in ./src will need to be passed to GCC so that it can find header files
#INC_DIRS := $(shell find $(SRC_DIRS) -type d)
# Add a prefix to INC_DIRS. So moduleA would become -ImoduleA. GCC understands this -I flag
#INC_FLAGS := $(addprefix -I,$(INC_DIRS))

CC := $(PATH)clang
LD := $(PATH)$(PREFIX)ld
OBJCOPY := $(PATH)$(PREFIX)objcopy
MINIPRO := /usr/bin/minipro #$(shell which minipro)

$(BUILD_DIR)/$(TARGET).bin: $(BUILD_DIR)/$(TARGET).elf
	$(OBJCOPY) $< -O binary $@

$(BUILD_DIR)/$(TARGET).elf: $(OBJS)
	$(LD) $(LDFLAGS) $(OBJS) -o $@

# Build step for C source
$(BUILD_DIR)/%.c.o: %.c
	$(CC) $(CFLAGS) -c $< -o $@

# Build step for C source
$(BUILD_DIR)/%.s.o: %.s
	$(CC) $(CFLAGS) -c $< -o $@

# Build step for C source
$(BUILD_DIR)/%.s.o: $(CRT_DIR)/%.s
	$(CC) $(CFLAGS) -c $< -o $@

.PHONY: flash
flash: $(BUILD_DIR)/$(TARGET).bin
	$(MINIPRO) -p $(ROMCHIP) -w $< -s

# # Build step for C++ source
# $(BUILD_DIR)/%.cpp.o: %.cpp
# 	mkdir -p $(dir $@)
# 	$(CXX) $(CPPFLAGS) $(CXXFLAGS) -c $< -o $@
