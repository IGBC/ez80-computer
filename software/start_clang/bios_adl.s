; Clock speed = 40MHz

.org 00h
.global start
start:
.ASSUME ADL=0
    JP.LIL _start

.fill 0Ah - 5
    .DW int_default ; PRT0
    .DW int_default ; PRT1
    .DW int_default ; PRT2
    .DW int_default ; PRT3
    .DW int_default ; PRT4
    .DW int_default ; PRT5
    .DW int_default ; RTC
    .DW int_default ; UART0
    .DW int_default ; UART1
    .DW int_default ; I2C
    .DW int_default ; SPI
.fill 16
    .DW int_default ; PORT B0
    .DW int_default ; PORT B1
    .DW int_default ; PORT B2
    .DW int_default ; PORT B3
    .DW int_default ; PORT B4
    .DW int_default ; PORT B5
    .DW int_default ; PORT B6
    .DW int_default ; PORT B7
    .DW int_default ; PORT C0
    .DW int_default ; PORT C1
    .DW int_default ; PORT C2
    .DW int_default ; PORT C3
    .DW int_default ; PORT C4
    .DW int_default ; PORT C5
    .DW int_default ; PORT C6
    .DW int_default ; PORT C7
    .DW int_default ; PORT D0
    .DW int_default ; PORT D1
    .DW int_default ; PORT D2
    .DW int_default ; PORT D3
    .DW int_default ; PORT D4
    .DW int_default ; PORT D5
    .DW int_default ; PORT D6
    .DW int_default ; PORT D7
.fill 16
.ASSUME ADL=1
int_default:
    reti

.section .text
wakeup_message:  
.db "eZ80 Single Board Computer Firmware 0.0.0-001", 10, 13 ,0
    .global _start
_start:
    ; set bottom of RAM to 2M
    LD A, 80h
    OUT0 (00ABh), A
    ; set top of ram to 4M
    LD A, 9Fh
    OUT0 (00ACh), A
    ; enable ram with 0WS
    LD A, 08h  ; write WS0 IO0 EN1
    OUT0 (00ADh), A
    ; shrink CSO so ram is unmasked
    LD A, 07h ; rom ends at 512K
    OUT0 (00A9h), A
    ; change rom settings
    LD A, 48h  ; write WS2 IO0 EN1
    OUT0 (00AAh), A
    
    ld a, 00h ; load pin 4 as output
    out0 (009Bh), A ; Write the value to the PORT B direction Register
    ld a, 00h ; write led low
    out0 (009Ah), A

;clear_ram:
;    ld a, 000h ; write led low
;    out0 (009Ah), A
;    ; Setup ram clear 
;    ld HL, 3FFFFFh ; Start at top of mem
;    ld BC, 200000h ; run trough all of mem
;    ld DE, 009Ah ; load the value we just loaded (0)
;    INDRX

setup_uart:
    ; set PD0 as alt mode
    ld a, 03h ; set bit 0
    out0 (00A2h), a ; PD_DR activate PD0
    out0 (00A3h), a ; PD_DDR activate PD0
    out0 (00A5h), a ; PD_ALT2 activate PD0 
    ld a, 0h 
    out0 (00A4h), a ; PD_ALT1 clear PD0 
    
    ld a, 0C0h
    out0 (00C3h), A ;activate access to BRG0
    
    ;set the uart divisor to 260 (9615 baud @ 40M clk)
    ld bc, 260 ;
    out0 (00C1h), B ; BRG0H
    out0 (00C0h), C ; BRG0L

    ld a, 03h ; No test settings, mode 8N1
    out0 (00C3h), A ; reactivate uart

    ld a, 06h ; enable fifo clear buffers
    out0 (00C2h), A ; UART0_FCTL

print_wakeup:
    ld hl, wakeup_message
    ld IX, print_done
    jp llcall_print_msg
print_done:

    ld SP, 9FFFFFh ;set up stack pointer

    ; call void main(void)
    call _main

signal_done:
    ld bc, 1
    
    in0 a, (009Ah)
    xor a, 10h ; toggle led bit
    out0 (009Ah), A
    
    ; ld a, 00h
    ; out0 (00A3h), a ; PD_DDR activate PD0
    ; in0 a, (00A2h)
    ; xor a, 0FFh ; toggle led bit
    ; out0 (00A2h), A
    
    ld hl, 100000
loop_done: sbc hl, bc  ; sub 1
    jr nz, loop_done
    
    jr signal_done

llcall_putchar:
    ld c, a
chwait: 
    in0 a, (0C5h)
    and a, 20h
    jr z, chwait
    ld a, c
    out0 (0C0h), A
    jp (IX)

llcall_print_msg:
    LEA DE, IX+0
    LD IX, prloop
prloop: ld a, (hl)
    and a, A
    jr z, prdone
    inc hl
    jr llcall_putchar
prdone:
    ex de, hl
    jp (hl)

; void ioOut(short addr, char val)
    .global _ioOut
_ioOut:
    push ix
    ld ix, 0
    add ix, sp
    ld bc, (ix+6)
    ld a,  (ix+9)
    out (bc), a
    ld sp, ix
    pop ix
    ret

; char ioIn(short addr)
   .global _ioIn
_ioIn:
    push ix
    ld ix, 0
    add ix, sp
    ld bc, (ix+6)
    in a, (bc)
    ld sp, ix
    pop ix
    ret

