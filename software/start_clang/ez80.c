#include "c0.h"

void *memset(void *ptr, int value, size_t num) {
    char *p = (char *)ptr;
    for (int i = 0; i < num; i++) {
        *p = (unsigned char)value; 
        p++;
    }
    return ptr;
}

extern  void ioOut (short addr, char data);

extern char ioIn (short addr);

static inline void putc(char c) {
    while (!(ioIn(0xC5) & 0x20));
    ioOut(0xC0, c);
}

static inline void puts(char *msg) {
    for (char i = 0; msg[i]; i++)
        putc(msg[i]);
}

void print_number(int n) {
    char out[8] = {0,0,0,0,0,0,0,0};
    // for (char i = 0; i < 8; i++)
    //     out[i] = 0;
    char i = 7;
    do {
        char d = n % 10;
        out[i] = d + '0';
        i--;
        n /= 10;
    } while (i>0 || n > 0);
    puts(&out[i]);
}

void main(void) {
    puts("Hello from C\r\n");
    int i = 0;
    while (1)
        print_number(i++);
}